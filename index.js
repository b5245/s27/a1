let http = require('http');

const port = 4000;

const server = http.createServer((req, res) => {
   if(req.url == "/profile"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end(`Welcome to your profile!`)
    }
    else if(req.url == "/courses"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end(`Here’s our courses available`)
    }
    else if(req.url == "/addcourse"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end(`Add a course to our resources`)
    }
    else if(req.url == "/updatecourse"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end(`Update a course to our resources`)
    }
    else if(req.url == "/archivecourse"){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end(`Archive courses to our resources`)
    }
    else{
        res.writeHead(404, {'Content-Type' : 'text/plain'})
        res.end(`Welcome to Booking System`)
    }
})

server.listen(port);

console.log(`Server running under localhost: ${port}`);
